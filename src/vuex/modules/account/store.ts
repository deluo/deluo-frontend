import Vue from 'vue';
import Vuex, { MutationTree, ActionTree } from 'vuex';
import { User } from '@/models/user';
import { Role } from '@/models/role';
import axios from 'axios';

Vue.use(Vuex);

interface State {
    currentUser: User;
    loggedIn: boolean;
    isAdmin: boolean;
    isLeader: boolean;
    isActive: boolean;
}

const mutations: MutationTree<State> = {
    setCurrentUser: (currentState, currentUser) => {
        currentState.currentUser = currentUser;
        currentState.currentUser.roles.forEach((role: Role) => {
            if (role.id === 1) {
                currentState.isAdmin = true;
            }
        });
        if (currentState.currentUser.active === false) {
            currentState.isActive = false;
        }
    },

    setApiToken: (currentState, apiToken) => {
        // Update the local storage by hand
        localStorage.removeItem('api_token');
        localStorage.setItem('api_token', apiToken);
        axios.defaults.headers.common.Authorization = 'Bearer '.concat(apiToken);
        currentState.loggedIn = true;
    },

    logout: (currentState) => {
        currentState.loggedIn = false;
        currentState.isAdmin = false;
        currentState.isLeader = false;
        currentState.isActive = true;
        currentState.currentUser = {} as User;
    },

    resetState() {
        // acquire initial state
        const s = initialState();

        Object.keys(s).forEach((key) => {
            state[key] = s[key];
        });
    },
};

const actions: ActionTree<State, any> = {};

const getters = {
    currentUser(currentState: State) {
        return currentState.currentUser;
    },
    loggedIn(currentState: State): boolean {
        return currentState.loggedIn;
    },
    isAdmin(currentState: State): boolean {
        return currentState.isAdmin;
    },
    isActive(currentState: State): boolean {
        return currentState.isActive;
    },
};

function initialState() {
    return {
        loggedIn: false,
        isAdmin: false,
        isLeader: false,
        isActive: true,
        currentUser: {} as User,
    };
}

const state: State = initialState();

const module = {
    namespaced: true,
    state,
    getters,
    mutations,
    actions,
};

export default module;
