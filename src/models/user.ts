import { Role } from './role';

export interface User {
    id: number;
    firstname: string;
    lastname: string;
    email: string;
    active: boolean;
    currentPassword: string;
    password: string;
    email_confirmation: string;
    roles: Role[];

    created_at: Date;
    updated_at: Date;
}
