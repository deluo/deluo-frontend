import { User } from './user';

export interface Department {
    id: number;
    title: string;
    description: string;
    entrance_year: number;
    token: string;
    users: User[];

    created_at: Date;
    updated_at: Date;
}
