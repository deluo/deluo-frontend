import { User } from './user';
import { Department } from './department';

export interface Assignment {
    id: number;
    department: Department;
    assignee: User;
    owner: User;
    remarks: string;
    start_date: Date;
    end_date: Date;

    created_at: Date;
    updated_at: Date;
}
