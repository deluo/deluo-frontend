import axios from 'axios';
import { User } from '@/models/user';

class UserWebservice {
    /**
     * Call the login method on api
     *
     * @param username Email address
     * @param password Password
     */
    public login(email: string, password: string) {
        return axios.post('/auth/login', {
            email,
            password,
        });
    }

    public register(user: User) {
        return axios.post('/auth/register', {
            firstname: user.firstname,
            lastname: user.lastname,
            email: user.email,
            password: user.password,
        });
    }

    public logout() {
        return axios.post('/auth/logout');
    }

    public getUserInfo() {
        return axios.get('/auth/userinfo');
    }

    public changeName(firstname: string, lastname: string) {
        return axios.post('account/updateName', {
            firstname,
            lastname,
        });
    }

    public confirmInfo(user: User) {
        return axios.post('/auth/confirmInfo', {
            firstname: user.firstname,
            lastname: user.lastname,
            email: user.email,
        });
    }

    public changeEmail(new_email: string) {
        return axios.post('account/updateEmail', {
            new_email,
        });
    }

    public changePassword(current_password: string, new_password: string) {
        return axios.post('account/updatePassword', {
            current_password,
            new_password,
        });
    }

    public confirmEmail(email_confirmation_token: string) {
        return axios.post('/auth/emailConfirmation', {
            email_confirmation_token,
        });
    }

    public getAll() {
        return axios.get('/admin/getAll');
    }

    public changeAccountInfo(user: User) {
        return axios.post('/auth/changeAccountInfo', {
            firstname: user.firstname,
            lastname: user.lastname,
            email: user.email,
            current_password: user.currentPassword,
            new_password: user.password,
        });
    }

    public update(user: User) {
        return axios.post('/admin/toggleUser', {
            id: user.id,
            active: user.active,
        });
    }
}

export const userWebservice = new UserWebservice();
