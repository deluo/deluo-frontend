import Vue from 'vue';
import Router from 'vue-router';

import Hochwacht from '@/components/Hochwacht.vue';


Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/',
            name: 'hochwacht',
            component: Hochwacht,
        },
    ],
});
