import Vue from 'vue';
import App from './App.vue';
import router from '@/router';
import store from '@/vuex/store';
import { VueEditor } from 'vue2-editor';

// import './registerServiceWorker';

// Main Sass file
import './assets/sass/main.scss';

import { Editor, EditorContent } from 'tiptap';

import { CalendarView, CalendarViewHeader } from 'vue-simple-calendar';

// Import MomentsJS
import moment from 'moment';

// Font awesome icons (4.7)
import 'font-awesome/css/font-awesome.min.css';

import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

Vue.component('font-awesome-icon', FontAwesomeIcon);

import 'tiptap';
// ===== vue-simple-calendar =====
import 'vue-simple-calendar/static/css/default.css';

import '@/assets/sass/hochwachtcalendar.scss';

Vue.use(CalendarView, CalendarViewHeader);

Vue.use(VueEditor);

// ===== vue-carousel =====
import VueCarousel from 'vue-carousel';

Vue.use(VueCarousel);

Vue.filter('formatDate', (value) => {
    if (value) {
        return moment(String(value)).format('DD.MM.YYYY');
    }
});

Vue.filter('formatShortDate', (value) => {
    if (value) {
        moment.locale('de-ch');
        return moment(String(value)).format('Do MMM');
    }
});

Vue.filter('formatShortDay', (value) => {
    if (value) {
        moment.locale('de-ch');
        return moment(String(value)).format('dddd. Do MMM');
    }
});

Vue.filter('formatTime', (value) => {
    if (value) {
        moment.locale('de-ch');
        return moment(String(value)).format('H:mm');
    }
});

// -- BUEFY --
// Vue JS Components for Bulma
import Buefy from 'buefy';
Vue.use(Buefy, {
    defaultIconPack: 'fas', // Maybe change to fas (new one)
});

// Import Axios for all network requests
import axios, { AxiosResponse } from 'axios';

// Fetch the token
const token = localStorage.getItem('api_token');

// If the token exist, set it in the authorization
if (token) {
    // Axios configuration
    axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
    axios.defaults.headers.common.Authorization = 'Bearer '.concat(token);
}

// Define the base api url
Vue.prototype.BACKEND_BASE_URL = process.env.VUE_APP_BACKEND_BASE_URL;
Vue.prototype.APP_BASE_URL = process.env.VUE_APP_BASE_URL;

axios.defaults.baseURL = process.env.VUE_APP_API_URL;

Vue.config.productionTip = false;

Vue.config.productionTip = false;

new Vue({
    router,
    store,
    render: (h) => h(App),
}).$mount('#app');
